# LFortran MVP Demo

This repository contains a Fortran library and several example programs showing
features that work with LFortran's MVP. This is using `fpm` to build and works
with both LFortran and GFortran.

The demo implements Gaussian quadrature of functions. It uses runtime
allocatable arrays, reads the input size from a file, computes something (an
integral of `sin(x)**2`) and writes the result to a file; a proxy for what a
scientific code would do.  LFortran does not yet support every Fortran feature
and so sometimes one must workaround the limitations, but enough features is
supported by now (see below) that it allows to write a full application that
computes something useful.  Interfacing with C works well, so one can implement
any currently missing features in C and call them from Fortran; the demo uses
this technique for file IO that is not yet implemented in LFortran.

# Instructions

Install LFortran and fpm:
```
conda create -n mvp_demo fpm lfortran
conda activate mvp_demo
```

Build, run and test `mvp_demo`:
```
fpm build --compiler=lfortran
fpm run --all --compiler=lfortran
fpm test --compiler=lfortran
```

# Fortran Features Used

The demo uses the following Fortran features:

* Program, modules, subroutines, functions
* Arrays, passing to functions as arguments, allocatable arrays
* Interfacing C using `iso_c_binding`, file IO implemented in C and wrapped
* Saving a mesh and values into a file
* Types: integer, real (double precision)
* If, select case, do loop
* Intrinsic functions: sin, abs, kind, trim, size

This is just a subset of features that already work with LFortran.  See the
[LFortran status page](https://docs.lfortran.org/progress/) for a comprehensive
list of features that LFortran supports.
