program main
  use mvp_demo, only: run
  implicit none

  call run()
end program main
