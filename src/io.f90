module io
use iso_c_binding, only: c_char, c_int, c_null_char, c_long_long, c_double
use iso_fortran_env, only: int64
implicit none
private
public :: open, close, write_i, write_rr, read_i

interface
    subroutine write_i(h, num) bind(C)
    import c_int,c_long_long
    integer(kind=c_long_long), value, intent(in) :: h
    integer(c_int), value, intent(in) :: num
    end subroutine

    subroutine read_i(h, num) bind(C)
    import c_int,c_long_long
    integer(kind=c_long_long), value, intent(in) :: h
    integer(c_int), intent(out) :: num
    end subroutine

    subroutine write_rr(h, r1, r2) bind(C)
    import c_long_long,c_double
    integer(kind=c_long_long), value, intent(in) :: h
    real(c_double), value, intent(in) :: r1, r2
    end subroutine

    subroutine close(h) bind(C)
    import c_long_long
    integer(kind=c_long_long), value, intent(in) :: h
    end subroutine
end interface

contains

function open(dirname, mode) result(h)
character(len=*),intent(in) :: dirname, mode
integer(int64) :: h
interface
    function c_open(filename, mode) bind(C, name="open") result(h)
    import c_char,c_long_long
    character(kind=c_char,len=1),intent(in) :: filename(*), mode(*)
    integer(kind=c_long_long) :: h
    end function
end interface
h = c_open(trim(dirname)//c_null_char, trim(mode)//c_null_char)
end function



end module
