module mvp_demo
use io, only: open, close, write_i, write_rr, read_i
use types, only: dp, int64
use quadrature, only: gauss_pts, gauss_wts
use constants, only: pi
implicit none
private
public :: run, integrate

contains

real(dp) function integrate(wtq, y) result(s)
real(dp), intent(in) :: wtq(:), y(:)
integer :: i
s = 0
do i = 1, size(y)
    s = s + y(i)*wtq(i)
end do
end function

subroutine run
integer(int64) :: h
integer :: i
integer :: Nq
real(dp) :: xmin, xmax, r1, r2, s
real(dp), allocatable :: xiq(:), wtq(:), x(:), y(:)
real(dp) :: jac, err

print *, "Reading the number of Gaussian quadrature points Nq from `input.txt`"
h = open("input.txt", "r")
call read_i(h, Nq)
call close(h)
print *, "Done. Nq =", Nq

xmin = 0
xmax = pi

allocate(xiq(Nq), wtq(Nq), x(Nq), y(Nq))

call gauss_pts(Nq, xiq)
call gauss_wts(Nq, wtq)

jac = (xmax-xmin)/2

do i = 1, Nq
    x(i) = (xiq(i)+1)*jac + xmin
    wtq(i) = wtq(i) * jac
end do

do i = 1, Nq
    r1 = x(i)
    y(i) = sin(r1)**2
end do

s = integrate(wtq, y)
err = abs(s - pi/2)

print *, "integrate(sin(x)**2, (x, 0, pi)) = ", s
print *, "Exact answer is pi/2             = ", pi/2
print *, "Error                            = ", err

print *, "Saving the quadrature grid and the values of sin(x) to `data.txt`..."
h = open("data.txt", "w")
do i = 1, Nq
    ! TODO: workaround for https://gitlab.com/lfortran/lfortran/-/issues/570
    r1 = x(i)
    r2 = y(i)
    call write_rr(h, r1, r2)
end do
call close(h)
print *, "Done"
end subroutine

end module
